#!/usr/bin/env python

from setuptools import setup

setup(
    name='Gostosinho Admin',
    version='1.0',
    description='Gostosinho App',
    author='Thiago Pio',
    author_email='thiago@businesstarget.com.br',
    url='http://www.python.org/sigs/distutils-sig/',
    install_requires=['Django==1.6.2','South==0.8.4','migrate==0.2.2','argparse==1.2.1','wsgiref==0.1.2'],
)

