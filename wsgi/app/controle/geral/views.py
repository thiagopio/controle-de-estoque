 # -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required


@login_required
def index(request):
	if not request.user.is_authenticated():
		return HttpResponse("You are logged in.")
	else:
		return HttpResponse("You are not logged in.")
