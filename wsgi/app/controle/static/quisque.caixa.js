$(document).ready(function(){
	$("#codigo_barra").focus();

	$("#caixa").bind('venda.visor', function(event, dados){
		console.log('venda.visor', dados);
		var html_sucesso = [
			'<ul>',
				'<li>{NOME}</li>',
				'<li>{VALOR_UND} x {QTD} = {VALOR_TOTAL}</li>',
			'</ul>'
		].join('');

		if (dados.erro == undefined) {
			$("#visor").html(
				html_sucesso.replace('{NOME}', dados.produto)
										.replace('{QTD}', dados.quantidade)
										.replace('{VALOR_UND}', formatReal(dados.valor_unidade))
										.replace('{VALOR_TOTAL}', formatReal(dados.valor_total))
			);
		} else {
			$("#visor").html(dados.erro);
		}
		$("#codigo_barra").focus();
	});

	$("#caixa").bind('venda.valor_total', function(event, valor_total){
		$("#total_cupom").val(formatReal(valor_total));
		$("#codigo_barra").focus();
	});

	$("#caixa").bind('venda.extrato', function(event, dados){
		console.log('venda.extrato', dados);
		var quantidade_itens = $("#lista_itens ul").size(),
			html_sucesso = [
			'<ul>',
				'<li class="id">#{ORDEM}</li>',
				'<li class="produto">{NOME}</li>',
				'<li class="quantidade">{QTD}</li>',
				'<li class="valor-unidade">{VALOR_UND}</li>',
				'<li class="valor-total">{VALOR_TOTAL}</li>',
			'</ul>'
		].join('');

		$("#lista_itens").append(
			html_sucesso.replace('{ORDEM}', quantidade_itens)
									.replace('{NOME}', dados.produto)
									.replace('{QTD}', dados.quantidade)
									.replace('{VALOR_UND}', formatReal(dados.valor_unidade))
									.replace('{VALOR_TOTAL}', formatReal(dados.valor_total))
		);
		$("#codigo_barra").focus();
	});

	$("#caixa").bind('venda.zerar_campos', function(event, dados){
		console.log('venda.cupom.novo', dados);
		$("#visor").empty();
		$("#lista_itens ul:gt(0)").remove();
		$("#qtd").val("1");
		$("#caixa").trigger("venda.valor_total", 0);
		$("#caixa").trigger('venda.cupom.gerar');
		$("#codigo_barra").focus();
	});

	$("#caixa").bind('venda.cupom.fechar', function(event, valores){
		console.log('venda.cupom.fechar', valores.valor_pago, valores.total_cupom);
		var valor_pago = valores.valor_pago;
		var total_cupom = valores.total_cupom;
		$.ajax({
			url: '/venda/servico/fechar_cupom',
			type: 'GET',
			cache: false,
			dataType: "json"
		}).done(function(dados) {
			 // var total_cupom = $("#total_cupom").val();
			 // var valor_pago = $("#valor_pago").val();

			 alert('TROCO: R$ ' + formatReal(valor_pago - total_cupom))
			$("#caixa").trigger('venda.zerar_campos', dados);
		}).fail(function(jqXHR, textStatus){
			console.log('error', jqXHR, textStatus);
		});
		$("#codigo_barra").focus();
	});

	$("#caixa").bind('venda.cupom.gerar', function(event){
		$.ajax({
			url: "/venda/servico/gerar_cupom",
			type: 'GET',
			cache: false,
			dataType: "json"
		}).done(function(dados) {
			console.log('gerar_cupom completo', dados);
		}).fail(function(jqXHR, textStatus){
			console.log('gerar_cupom error', jqXHR, textStatus);
		});
	});

	$("#caixa").bind('venda.item.cancelar', function(event){
		var ordem = parseInt(prompt("Numero do item para ser cancelado"),10);
		if (ordem > 0) {
			$.ajax({
				url: '/venda/servico/cancelar/item/'+ordem+'/',
				type: 'GET',
				cache: false,
				dataType: "json"
			}).done(function(dados) {
				$($("#lista_itens ul")[ordem]).addClass('item-cancelado');
				$("#caixa").trigger("venda.valor_total", dados.cupom_total);
			}).fail(function(jqXHR, textStatus){
				console.log('error', jqXHR, textStatus);
			});
		}
		$("#codigo_barra").focus();
	});

	$("#caixa").bind('venda.cupom.cancelar', function(event){
		var numero = parseInt(prompt("Numero do cupom para ser cancelado"),10);
		if (numero > 0) {
			$.ajax({
				url: '/venda/servico/cancelar/cupom/'+numero+'/',
				type: 'GET',
				cache: false,
				dataType: "json"
			}).done(function(dados) {
				console.log(dados);
				alert("cupom cancelado com sucesso");
			}).fail(function(jqXHR, textStatus){
				if (jqXHR.status == 404) {
					alert("CUPOM NAO ENCONTRADO");
				}
				console.log('error', jqXHR, textStatus);
			});
		}
		$("#codigo_barra").focus();
	});

	$("#caixa").bind('venda.item.adicionar', function(event, dados){
		console.log('venda.item.adicionar', dados);
		$.ajax({
			url: '/venda/servico/adicionar/'+dados.quantidade+'/'+dados.codigo,
			type: 'GET',
			cache: false,
			dataType: "json"
		}).done(function(dados) {
			$("#codigo_barra").val("");
			$("#qtd").val("1");
			$("#caixa").trigger("venda.visor", dados);
			if (dados.erro == undefined) {
				$("#caixa").trigger("venda.extrato", dados);
				$("#caixa").trigger("venda.valor_total", dados.cupom_total);
			}
		}).fail(function(jqXHR, textStatus){
			console.log('error', jqXHR, textStatus);
		});
		$("#codigo_barra").focus();
	});

	$("#caixa").bind('venda.item.consultar', function(event, codigo){
		console.log('venda.item.consultar', codigo);
		$.ajax({
			url: '/venda/servico/consultar/'+codigo,
			type: 'GET',
			cache: false,
			dataType: "json"
		}).done(function(dados) {
			$("#caixa").trigger("venda.visor", dados);
		}).fail(function(jqXHR, textStatus){
			console.log('error', jqXHR, textStatus);
		});
		$("#codigo_barra").focus();
	});

	$("#caixa").bind('venda.fechar_caixa', function(event){
		document.location="/venda/servico/fechar_caixa"
	});

	$("#caixa").bind('venda.item.quantidade', function(event){
		var quantidade = parseInt(prompt("Digite a quantidade"),10);
		if (quantidade > 0) {
			$("#qtd").val(quantidade);
		}
		$("#codigo_barra").focus();
	});

});