function formatReal( int ) {
    var tmpNeg = int+'';        
    var int = parseInt(int.toFixed(2).toString().replace(/[^\d]+/g, ''));
    var tmp = int+'';
    var neg = false;
    if(tmpNeg.indexOf("-") == 0) {
        neg = true;
        tmp = tmp.replace("-","");
    }
    if(tmp.length == 1) tmp = "0"+tmp
    tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
    if( tmp.length > 6)
    tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
    if( tmp.length > 9)
    tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2,$3");
    if( tmp.length > 12)
    tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2.$3,$4");
    if(tmp.indexOf(".") == 0) tmp = tmp.replace(".","");
    if(tmp.indexOf(",") == 0) tmp = tmp.replace(",","0,");
    return (neg ? '-'+tmp : tmp);
}

function formatUniversal( int ) {
    console.log(int);
    // int = (int == '' ? 0 : int);
    var tmpNeg = int+'';
    var int = parseInt(int.toFixed(2).toString().replace(/[^\d]+/g, ''));
    var tmp = int+'';
    var neg = false;
    if(tmpNeg.indexOf("-") == 0) {
	    neg = true;
	    tmp = tmp.replace("-","");
    }
    if(tmp.length == 1) tmp = "0"+tmp
    tmp = tmp.replace(/([0-9]{2})$/g, ".$1");
    if( tmp.length > 6)
    tmp = tmp.replace(/([0-9]{3}).([0-9]{2}$)/g, ".$1.$2");
    if( tmp.length > 9)
    tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{2}$)/g,".$1.$2.$3");
    if( tmp.length > 12)
    tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}).([0-9]{2}$)/g,".$1.$2.$3.$4");
    // if(tmp.indexOf(".") == 0) tmp = tmp.replace(".","");
    if(tmp.indexOf(".") == 0) tmp = tmp.replace(".","0.");
    return (neg ? '-'+tmp : tmp);
}

function moeda2float(moeda){
    // moeda = typeof(moeda) == 'number' ? float2moeda(moeda) : String(moeda);
    moeda = moeda.replace(".","");
    moeda = moeda.replace(",",".");
    return parseFloat(moeda);
}

function float2moeda(num) {
    x = 0;
    if(num<0) {
      num = Math.abs(num);
      x = 1;
    }
    if(isNaN(num)) num = "0";
      cents = Math.floor((num*100+0.5)%100);
    num = Math.floor((num*100+0.5)/100).toString();
    if(cents < 10) cents = "0" + cents;
      for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
         num = num.substring(0,num.length-(4*i+3))+'.'
               +num.substring(num.length-(4*i+3));
    ret = num + ',' + cents;
    if (x == 1) ret = ' - ' + ret; return ret;
}