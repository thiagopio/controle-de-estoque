(function($) {
    $(document).ready(function($) {
        // django.jQuery(".field-preco_custo_unidade input, .field-preco_custo_caixa input").attr("readonly");
        django.jQuery("#id_loja").on("change", function(){
            django.jQuery(".dynamic-entradaprodutos_set input").trigger("change")
        });
        django.jQuery(".dynamic-entradaprodutos_set, #id_loja").on("change", "input, select", function(){
            var loja_id = django.jQuery(".field-loja option:selected").val(),
                $linha = django.jQuery(this).parents("tr"),
                produto_id = $linha.find(".field-produto option:selected").val(),
                $quantidade_caixa = $linha.find(".field-quantidade_caixa input"),
            	$preco_custo_unidade = $linha.find(".field-preco_custo_unidade input"),
            	$preco_custo_caixa = $linha.find(".field-preco_custo_caixa input"),
                $preco_custo_total = $linha.find(".field-preco_custo_total input"),
            	$margem_lucro = $linha.find(".field-margem_custo_unidade input"),
                valor_custo_caixa, valor_custo_unidade, valor_margem;

            produto_id = (produto_id == "" ? 0 : parseInt(produto_id,10));
            loja_id = (loja_id == "" ? 0 : parseInt(loja_id,10));
        	valor_custo_caixa = $preco_custo_total.val() / $quantidade_caixa.val();
        	$preco_custo_caixa.val(+(Math.round(valor_custo_caixa + "e+2")  + "e-2"));

            if (produto_id && loja_id) {
                $preco_custo_unidade.val("consultando...");
                $margem_lucro.val("consultando...");

                $.ajax({
                    url: '/estoque/servico/'+loja_id+'/'+produto_id+'/verificar_quantidade_e_custo',
                    type: 'GET',
                    cache: false,
                    dataType: "json"
                }).done(function(dados) {
                    console.log('sucesso', dados);
                    valor_margem = (dados.ultimo_preco_custo_unidade == "Null" ? 0 : dados.ultimo_preco_custo_unidade);
                    valor_custo_unidade = $preco_custo_caixa.val() / dados.quantidade_por_caixa;
                    $preco_custo_unidade.val(+(Math.round(valor_custo_unidade + "e+2")  + "e-2"));
                    if (valor_margem == 0) {
                        $margem_lucro.val(0);
                    } else {
                        valor_margem = $preco_custo_unidade.val() - valor_margem;
                        $margem_lucro.val(+(Math.round(valor_margem + "e+2")  + "e-2"));
                    }

                    // $preco_custo_unidade.val(formatUniversal($preco_custo_unidade.val()));
                    // $margem_lucro.val(formatUniversal($margem_lucro.val()));
                    // $preco_custo_caixa.val(formatUniversal($preco_custo_caixa.val()));
                    // $preco_custo_total.val(formatUniversal($preco_custo_total.val()));
                }).fail(function(jqXHR, textStatus){
                    console.log('error', jqXHR, textStatus);
                });
            }
        });
    });
})(django.jQuery);