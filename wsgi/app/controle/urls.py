from django.conf.urls import patterns, include, url
# from django.views.generic import RedirectView
from django.contrib import admin
from django.contrib.auth import views as auth_views

admin.autodiscover()

urlpatterns = patterns('',
  url(r'^estoque/', include('controle.estoque.urls')),
  url(r'^venda/', include('controle.venda.urls')),
  url(r'^relatorio/', include('controle.relatorio.urls')),
  url(r'^admin/', include(admin.site.urls)),
  url(r'^accounts/login/$', auth_views.login),
  # url(r'^/', 'controle.geral.views.index'),
  # url(r'^caixa/', RedirectView.as_view(url='/venda/caixa')),
)
