 # -*- coding: utf-8 -*-
from controle.venda.models import Caixa
from controle.estoque.models import HistoricoEstoque, Loja, Produto, Fornecedor, ProdutoLojaEstoque
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render
import datetime
# from django.db.models import Sum


@login_required
def detalhe_estoque_por_produto(request, loja, produto, data, modelo):
	loja = get_object_or_404(Loja, pk=loja)
	produto = get_object_or_404(Produto, pk=produto)
	historico_produtos_x_loja = HistoricoEstoque.objects.filter(loja=loja, produto=produto, data_cadastro=data, origem_model=modelo).order_by('-id')
	data = datetime.datetime.strptime(data, '%Y-%m-%d')
	return render(request, 'detalhe_estoque.html', {
		"loja": loja,
		"produto": produto,
		"data_cadastro": data,
		"listagem": historico_produtos_x_loja
	})

@login_required
def detalhe_caixa(request, caixa):
	caixa = get_object_or_404(Caixa, pk=caixa)
	return render(request, 'detalhe_caixa.html', {"caixa": caixa})

@login_required
def relatorio_entradas(request):
	fornecedores = Fornecedor.objects.all().order_by('nome')
	lojas = Loja.objects.all().order_by('nome')
	produtos = Produto.objects.all().order_by('nome')
	loja = int(request.GET['loja']) if request.GET.has_key('loja') else None
	data_inicial = request.GET['data_inicial'] if request.GET.has_key('data_inicial') else ''
	data_final = request.GET['data_final'] if request.GET.has_key('data_final') else ''
	mensagem_erro = None

	try:
		data_inicial = datetime.datetime.strptime(data_inicial, '%d/%m/%Y') if data_inicial != '' else ''
	except Exception:
		data_inicial = ""
		mensagem_erro = 'Data inicial com formato inválido'

	try:
		data_final = datetime.datetime.strptime(data_final, '%d/%m/%Y') if data_final != '' else ''
	except Exception:
		data_final = ""
		mensagem_erro = 'Data final com formato inválido'

	entradas = HistoricoEstoque.objects.filter(quantidade__gt=0)
	entradas = entradas.filter(loja__id=loja) if loja > 0 else entradas;
	entradas = entradas.filter(data_cadastro__gte=data_inicial.strftime('%Y-%m-%d')) if data_inicial != '' else entradas;		
	entradas = entradas.filter(data_cadastro__lte=(data_final+datetime.timedelta(hours=24)).strftime('%Y-%m-%d')) if data_final != '' else entradas;		

	return render(request, 'relatorio_entradas.html', {
		"loja_f": loja,
		"data_inicial_f": data_inicial,
		"data_final_f": data_final,
		"itens": entradas.order_by('-id'),
		"fornecedores": fornecedores,
		"lojas": lojas,
		"produtos": produtos,
		"erro": mensagem_erro
	})

@login_required
def relatorio_vendas(request):
	lojas = Loja.objects.all().order_by('nome')
	loja = int(request.GET['loja']) if request.GET.has_key('loja') else None
	data_inicial = request.GET['data_inicial'] if request.GET.has_key('data_inicial') else ''
	data_final = request.GET['data_final'] if request.GET.has_key('data_final') else ''
	mensagem_erro = None

	try:
		data_inicial = datetime.datetime.strptime(data_inicial, '%d/%m/%Y') if data_inicial != '' else ''
	except Exception:
		data_inicial = ""
		mensagem_erro = 'Data inicial com formato inválido'

	try:
		data_final = datetime.datetime.strptime(data_final, '%d/%m/%Y') if data_final != '' else ''
	except Exception:
		data_final = ""
		mensagem_erro = 'Data final com formato inválido'

	resultado = Caixa.objects.all()
	resultado = resultado.filter(loja__id=loja) if loja > 0 else resultado;
	resultado = resultado.filter(data_inicial__gte=data_inicial.strftime('%Y-%m-%d')) if data_inicial != '' else resultado;		
	resultado = resultado.filter(data_inicial__lte=(data_final+datetime.timedelta(hours=24)).strftime('%Y-%m-%d')) if data_final != '' else resultado;

	return render(request, 'relatorio_vendas.html', {
		"loja_f": loja,
		"data_inicial_f": data_inicial,
		"data_final_f": data_final,
		"lojas": lojas,
		"itens": resultado.order_by('-id'),
		"erro": mensagem_erro
	})

@login_required
def relatorio_estoque_atual(request):
	lojas = Loja.objects.all().order_by('nome')
	loja = int(request.GET['loja']) if request.GET.has_key('loja') else None

	# saidas = HistoricoEstoque.objects.filter(quantidade__lte=0).values('loja__id', 'loja__nome', 'produto__id', 'produto__nome', 'data_cadastro', 'origem_model').annotate(Sum('quantidade'))
	# entradas = HistoricoEstoque.objects.filter(quantidade__gte=0).values('loja__id', 'loja__nome', 'produto__id', 'produto__nome', 'data_cadastro', 'origem_model').annotate(Sum('quantidade'))
	
	resultado = ProdutoLojaEstoque.objects.all().order_by('produto__nome','loja__nome')
	resultado = resultado.filter(loja__id=loja) if loja > 0 else resultado;

	return render(request, 'relatorio_estoque_atual.html', {
		"loja_f": loja,
		"itens": resultado,
		"lojas": lojas
	})
