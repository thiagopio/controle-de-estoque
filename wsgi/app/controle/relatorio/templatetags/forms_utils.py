from django import template

register = template.Library()

@register.filter
def selected(value, arg):
  if value == arg:
    return 'selected'
