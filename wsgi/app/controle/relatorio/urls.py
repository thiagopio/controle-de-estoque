from django.conf.urls import patterns, url
from controle.relatorio import views

urlpatterns = patterns('',
    url(r'^estoque/(?P<loja>\d+)/(?P<produto>\d+)/(?P<data>[0-9-]+)/(?P<modelo>\w+)/$', views.detalhe_estoque_por_produto),
    url(r'^caixa/(?P<caixa>\d+)/$', views.detalhe_caixa),
    url(r'^entradas/$', views.relatorio_entradas),
    url(r'^vendas/$', views.relatorio_vendas),
    url(r'^estoque/$', views.relatorio_estoque_atual),
)
