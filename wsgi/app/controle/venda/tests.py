from django.test import TestCase
from controle.estoque.models import Loja
from controle.venda.models import Caixa
from django.contrib.auth.models import User


class CaixaTestCase(TestCase):
	fixtures = ['loja','user']

	def test_fundo_caixa(self):
		user = User.objects.get(pk=1)
		loja = Loja.objects.get(pk=1)
		self.assertEquals(loja.fundo_de_caixa, None)
		Caixa.objects.create(
			usuario=user, 
			loja=loja, 
			fundo_caixa_inicial=10,
			valor_vendas=0,
			valor_despesa=0,
			valor_consumo_interno=0,
			fundo_caixa_final=10
		).calcular_fechamento()
		self.assertEquals(loja.fundo_de_caixa, 10)