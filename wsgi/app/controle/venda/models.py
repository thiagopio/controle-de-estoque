 #!/usr/bin/python
 # -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from controle.estoque.models import Loja, ProdutoPreco
from django.utils.html import format_html
from decimal import Decimal


class Caixa(models.Model):
	usuario = models.ForeignKey(User)
	loja = models.ForeignKey(Loja)
	fundo_caixa_inicial = models.DecimalField(verbose_name="Fundo inicial", max_digits=5, decimal_places=2, blank=False)
	fundo_caixa_final = models.DecimalField(verbose_name="Fundo final", max_digits=5, decimal_places=2, null=True, blank=True)
	valor_despesa = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=False)
	valor_consumo_interno = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
	valor_vendas = models.DecimalField(verbose_name="Total vendido", max_digits=5, decimal_places=2, null=True, blank=False)
	valor_entregue = models.DecimalField(verbose_name="Total em Caixa", max_digits=5, decimal_places=2, null=True, blank=False)
	valor_final = models.DecimalField(verbose_name="Total Caixa", max_digits=5, decimal_places=2, null=True, blank=False)
	data_inicial = models.DateTimeField(verbose_name="Data abertura", auto_now_add=True)
	data_final = models.DateTimeField(verbose_name="Data fechamento", null=True, blank=True)
	data_atualizacao = models.DateTimeField(auto_now=True)

	def extrato_completo(self):
		return format_html(u'<a href="/relatorio/caixa/{0}/" target="_blank">detalhamento</a>', self.id)

	def extrato_caixa(self):
		if self.data_final == None:
			return ''
		else:
			return format_html(u'<a href="/venda/impressao/caixa/{0}/" target="_blank">fechamento</a>', self.id)			

	def calcular_fechamento(self):
		self.valor_final = self.valor_entregue - (self.valor_despesa + self.fundo_caixa_final)
		self.save()
		self.loja.fundo_de_caixa = self.fundo_caixa_final
		self.loja.save()

	def calcular_total_vendas(self):
		self.valor_vendas = 0
		for cupom in self.cupons():
			self.valor_vendas += cupom.valor_total

	def calcular_total_vendedor(self):
		if (self.valor_entregue - self.valor_vendas) <= 0:
			self.valor_final = (self.valor_vendas - self.fundo_caixa_final)
		else: 
			self.valor_final = (self.valor_entregue - self.fundo_caixa_final)
		self.valor_final -= self.valor_despesa

	def cupons(self):
		return Cupom.objects.filter(caixa=self, pago=True)

	def cupons_pagos(self):
		Cupom.objects.filter(caixa=self, cancelado=False, pago=True)
	
	def cupons_cancelados(self):
		Cupom.objects.filter(caixa=self, cancelado=True)

	def __unicode__(self):
		return "%s - %d" % (self.loja.nome, self.id)


class Cupom(models.Model):
	caixa = models.ForeignKey(Caixa)
	total_itens = models.PositiveIntegerField(default=0, null=True)
	valor_total = models.DecimalField(max_digits=5, decimal_places=2, blank=True, default=0)
	valor_desconto = models.DecimalField(max_digits=5, decimal_places=2, blank=True, default=0)
	data_cadastro = models.DateTimeField(auto_now_add=True)
	data_atualizacao = models.DateTimeField(auto_now=True)
	cancelado = models.BooleanField(default=False)
	pago = models.BooleanField(default=False)

	def itens_todos(self):
		return Item.objects.filter(cupom=self)

	def itens_validos(self):
		return Item.objects.filter(cupom=self, cancelado=False)

	def cancelar(self):
		itens = Item.objects.filter(cupom=self)
		for item in itens:
			if not item.cancelado:
				item.cancelar()
		self.cancelado = True
		self.save()

	def adicionar(self, item):
		self.total_itens += 1
		self.valor_total += Decimal(item.preco_total)
		self.save()

	def remover(self, item):
		self.total_itens -= 1
		self.valor_total -= Decimal(item.preco_total)
		self.save()

	def finalizar(self):
		self.pago = True
		self.save()

	def __unicode__(self):
		return "%0*d" % (6, self.id)


class Item(models.Model):
	cupom = models.ForeignKey(Cupom)
	produto_preco = models.ForeignKey(ProdutoPreco)
	quantidade =  models.PositiveIntegerField()
	preco_unidade = models.DecimalField(max_digits=5, decimal_places=2)
	preco_total = models.DecimalField(max_digits=5, decimal_places=2)
	cancelado = models.BooleanField(default=False)

	def cancelar(self):
		self.produto_preco.devolver(self.quantidade)
		self.cancelado = True
		self.save()

	def __unicode__(self):
		return self.produto_preco.produto_venda.nome
