# from django import forms
from django.forms import ModelForm
# from controle.estoque.models import Loja
from controle.venda.models import Caixa


class CaixaFinalForm(ModelForm):
	class Meta:
		model = Caixa
		fields = ['fundo_caixa_final', 'valor_despesa', 'valor_consumo_interno', 'valor_entregue']

	# def clean_fundo_caixa_final(self):
	# 	print "xxxxxxx"

class CaixaInicialForm(ModelForm):
	class Meta:
		model = Caixa
		fields = ['loja', 'fundo_caixa_inicial']
