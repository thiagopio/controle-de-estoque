from django.conf.urls import patterns, url
from controle.venda import views

urlpatterns = patterns('',
    # url(r'^cupom$', views.gerar_cupom),

    url(r'^servico/gerar_cupom', views.gerar_cupom),
    url(r'^servico/fechar_cupom', views.fechar_cupom),
    url(r'^servico/consultar/(?P<codigo_produto>\d+)/$', views.consultar_item),
    url(r'^servico/adicionar/(?P<quantidade>\d+)/(?P<codigo_produto>\d+)/$', views.adicionar_item),
    url(r'^servico/cancelar/item/(?P<ordem>\d+)/$', views.cancelar_item),
    url(r'^servico/cancelar/cupom/(?P<numero>\d+)/$', views.cancelar_cupom),
    url(r'^servico/fechar_caixa', views.fechar_caixa),

    url(r'^impressao/cupom/(?P<numero>\d+)/$', views.impressao_cupom),
    url(r'^impressao/caixa/(?P<id>\d+)/$', views.impressao_caixa),
    
    url(r'^caixa/fechar', views.formulario_fechar_caixa),
    url(r'^sessao_caixa', views.sessao_caixa),
    url(r'^caixa$', views.caixa, name='caixa'),
)