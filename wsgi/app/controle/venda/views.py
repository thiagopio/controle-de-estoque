 # -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from controle.estoque.models import ProdutoPreco, Loja
from controle.venda.models import Caixa, Cupom, Item
from controle.venda.forms import CaixaInicialForm, CaixaFinalForm
from django.core import serializers
from decimal import Decimal
from datetime import datetime
import json


@login_required
def gerar_cupom(request):
	# https://docs.djangoproject.com/en/dev/topics/auth/default/#auth-web-requests
	# usuario = request.user
	caixa = Caixa.objects.get(pk=request.session['caixa'])
	cupom, novo_cupom = Cupom.objects.get_or_create(caixa=caixa, pago=False)
	response = {}
	response['cupom'] = cupom.id
	request.session['cupom'] = cupom.id
	return HttpResponse(json.dumps(response), content_type = 'application/json')

@login_required
def fechar_cupom(request):
	cupom = Cupom.objects.get(pk=request.session['cupom'])
	cupom.finalizar()
	cupom_json = serializers.serialize("json", [cupom])
	return HttpResponse(cupom_json, content_type='json')

@login_required
def consultar_item(request, codigo_produto):
	response = {}
	try:
		loja = Loja.objects.get(pk=request.session['loja'])
		produto_preco = ProdutoPreco.objects.get(produto_venda__codigo=codigo_produto, loja=loja)

		response['produto'] = produto_preco.produto_venda.nome
		response['quantidade'] = 1
		response['valor_unidade'] = float(produto_preco.preco_loja)
		response['valor_total'] = response['valor_unidade']

	except ProdutoPreco.DoesNotExist:
		response['erro'] = 'preco do produto nao encontrado'

	return HttpResponse(json.dumps(response), content_type = 'application/json')

@login_required
def adicionar_item(request, quantidade, codigo_produto):
	response = {}
	quantidade = int(quantidade)
	loja = Loja.objects.get(pk=request.session['loja'])
	try:
		cupom = Cupom.objects.get(pk=request.session['cupom'])
		produto_preco = ProdutoPreco.objects.get(produto_venda__codigo=codigo_produto, loja=loja)

		produto_preco.consumir(quantidade)
		response['produto'] = produto_preco.produto_venda.nome
		response['quantidade'] = quantidade
		response['valor_unidade'] = float(produto_preco.preco_loja)
		response['valor_total'] = float(produto_preco.preco_loja * quantidade)

		item = Item.objects.create(produto_preco=produto_preco, cupom=cupom, quantidade=quantidade, preco_unidade=str(response['valor_unidade']), preco_total=str(response['valor_total']))
		cupom.adicionar(item)
		response['cupom_total'] = float(cupom.valor_total)

	except ProdutoPreco.DoesNotExist:
		response['erro'] = 'preco do produto nao encontrado'
	# except Exception, e:
	# 	response['erro'] = 'problema interno'
		
	return HttpResponse(json.dumps(response), content_type = 'application/json')

@login_required
def cancelar_item(request, ordem):
	response = {}
	cupom = Cupom.objects.get(pk=request.session['cupom'])
	itens = cupom.itens_todos()
	item = itens[int(ordem)-1]
	if not item.cancelado:
		item.cancelar();
		cupom.remover(item)
	response['cupom_total'] = float(cupom.valor_total)
	return HttpResponse(json.dumps(response), content_type = 'application/json')

@login_required
def cancelar_cupom(request, numero):
	response = {}
	cupom = get_object_or_404(Cupom, pk=numero, caixa__id=request.session['caixa'], pago=True, cancelado=False)
	cupom.cancelar()
	return HttpResponse(json.dumps(response), content_type = 'application/json')

@login_required
def impressao_cupom(request, numero):
	cupom = get_object_or_404(Cupom, pk=numero, pago=True)
	return render(request, 'impressao_cupom.html', {"cupom": cupom})

@login_required
def impressao_caixa(request, id):
	caixa = get_object_or_404(Caixa, pk=id)
	if caixa.data_final is None:
		return caixa(request)
	elif caixa.valor_final is None:
		return fechar_caixa(request)
	caixa.calcular_total_vendedor()
	caixa.save()
	return render(request, 'impressao_caixa.html', {
		"caixa": caixa,
		"saldo_vendedor": (caixa.valor_entregue - caixa.valor_despesa - (caixa.valor_vendas + caixa.fundo_caixa_inicial))
	})

@login_required
def sessao_caixa(request):
	form = CaixaInicialForm(request.POST)
	if form.is_valid():
		caixa_aberto = Caixa.objects.filter(usuario=request.user, valor_final=None).last()
		if caixa_aberto:
			print "CAIXA ABERTO > sessao_caixa()"
			request.session['loja'] = caixa_aberto.loja.id
			request.session['caixa'] = caixa_aberto.id
		else:
			loja = Loja.objects.get(pk = int(form.data['loja']))
			caixa = Caixa.objects.create(usuario=request.user, loja=loja, fundo_caixa_inicial=form.data['fundo_caixa_inicial'])
			request.session['loja'] = loja.id
			request.session['caixa'] = caixa.id
		return HttpResponseRedirect('/venda/caixa')
	else:
		return abrir_caixa(request, form)

@login_required
def fechar_caixa(request):
	caixa = Caixa.objects.get(pk=request.session['caixa'])
	if caixa.data_final == None:
		caixa.calcular_total_vendas()
		caixa.data_final = datetime.now()
		caixa.save()
	return HttpResponseRedirect('/venda/caixa/fechar')

@login_required
def formulario_fechar_caixa(request):
	if not request.session.has_key('caixa'):
		return HttpResponseRedirect('/venda/caixa')
	caixa = Caixa.objects.get(pk=request.session['caixa'])
	if request.POST:
		form = CaixaFinalForm(request.POST)
		if form.is_valid():
			caixa.valor_entregue = Decimal(form.data['valor_entregue'])
			caixa.fundo_caixa_final = Decimal(form.data['fundo_caixa_final'])
			caixa.valor_despesa = Decimal(form.data['valor_despesa'])
			caixa.calcular_fechamento()

			request.session.pop('loja')
			request.session.pop('caixa')
			request.session.pop('cupom')
			return HttpResponseRedirect('/venda/impressao/caixa/%d' % caixa.id)
		else:
			return render(request, 'caixa_form_fechar.html', {"form": form})

	form = CaixaFinalForm(instance=caixa)
	return render(request, 'caixa_form_fechar.html', {"form": form, "caixa": caixa})

@login_required
def abrir_caixa(request, form = None):
	caixa_aberto = Caixa.objects.filter(usuario=request.user, valor_final=None).last()
	if caixa_aberto:
		print "CAIXA ABERTO > abrir_caixa()"
		request.session['loja'] = caixa_aberto.loja.id
		request.session['caixa'] = caixa_aberto.id
		request.session['cupom'] = Cupom.objects.filter(caixa=caixa_aberto).last().id
		return HttpResponseRedirect('/venda/caixa')	
	return render(request, 'caixa_form_abrir.html', {"form": form})

@login_required
def caixa(request):
	if not request.session.has_key('loja') or not request.session.has_key('caixa'):
		form = CaixaInicialForm()
		return abrir_caixa(request, form)
	
	if request.session.has_key('cupom'):
		cupom = Cupom.objects.get(pk=request.session['cupom'])
	else:
		cupom = None

	return render(request, 'caixa.html', {"cupom": cupom})
