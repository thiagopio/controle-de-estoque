# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from controle.estoque.models import Produto, ProdutoCustoLoja
import json


@login_required
def verificar_quantidade_e_custo(request, loja_id, produto_id):
	response = {}
	produto = Produto.objects.get(pk=produto_id)
	try:
		produto_custo_loja = ProdutoCustoLoja.objects.get(produto=produto, loja__id=loja_id)
	except ProdutoCustoLoja.DoesNotExist:
		produto_custo_loja = None

	response['quantidade_por_caixa'] = produto.quantidade_por_caixa if produto != None else 0
	response['ultimo_preco_custo_unidade'] = float(produto_custo_loja.ultimo_custo_por_unidade)  if produto_custo_loja != None else 0
	return HttpResponse(json.dumps(response), content_type = 'application/json')
