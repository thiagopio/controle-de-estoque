 #!/usr/bin/python
 # -*- coding: utf-8 -*-
from django.db import models
from django.db.models.signals import post_save
from django.db.models.loading import get_model
from django.dispatch import receiver
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
# from django.utils.html import format_html


class TipoFornecedor(models.Model):
	nome = models.CharField(max_length=100)

	class Meta:
		verbose_name = u'Tipo de Fornecedor'
		verbose_name_plural = u'Tipos de Fornecedor'

	def __unicode__(self):
		return self.nome

class Fornecedor(models.Model):
	nome = models.CharField(max_length=100)
	sigla = models.CharField(max_length=4, blank=False)
	tipo = models.ForeignKey(TipoFornecedor)
	cnpj = models.CharField(verbose_name="CNPJ", max_length=15, null=True, help_text=u"somente números no campo acima")
	responsavel = models.CharField(verbose_name="Responsável", max_length=100, null=True)
	endereco = models.CharField(verbose_name="Endereço", max_length=150, null=True)
	bairro = models.CharField(max_length=100, null=True)
	cep = models.CharField(verbose_name="CEP", max_length=8, null=True, help_text=u"somente números no campo acima")
	data_cadastro = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return "%s - %s" % (self.sigla, self.nome)

	class Meta:
		verbose_name = 'Fornecedor'
		verbose_name_plural = 'Fornecedores'

class FornecedorContato(models.Model):
	TIPOS = (
		('FIXO','Fixo'),
		('CEULAR','Celular'),
		('EMAIL','E-mail'),
	)
	fornecedor = models.ForeignKey(Fornecedor)
	tipo = models.CharField(max_length=10, choices=TIPOS)
	contato = models.CharField(max_length=50)
	observacao = models.CharField(verbose_name="Observação", max_length=100, null=True, blank=True)

	def __unicode__(self):
		return "%s # %s" % (self.tipo, self.contato)

class Loja(models.Model):
	nome = models.CharField(max_length=100)
	cnpj = models.CharField(verbose_name="CNPJ", max_length=15, null=True, blank=True, help_text=u"somente números no campo acima")
	endereco = models.CharField(verbose_name="Endereço", max_length=150, null=True, blank=True)
	bairro = models.CharField(max_length=50, blank=True)
	cep = models.CharField(verbose_name="CEP", max_length=8, null=True, blank=True, help_text=u"somente números no campo acima")
	data_cadastro = models.DateTimeField(auto_now_add=True)
	fundo_de_caixa =  models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)

	def __unicode__(self):
		return self.nome

class TipoProduto(models.Model):
	nome = models.CharField(max_length=100)
	
	class Meta:
		verbose_name = u'Tipo de Produto'
		verbose_name_plural = u'Tipos de Produto'
		
	def __unicode__(self):
		return self.nome

class Produto(models.Model):
	nome = models.CharField(max_length=100)
	tipo = models.ForeignKey(TipoProduto)
	quantidade_por_caixa = models.PositiveIntegerField(blank=False)
	quantidade_caixa_minima = models.PositiveIntegerField(default=1)
	
	def __unicode__(self):
		return "%s" % self.nome


class ProdutoVenda(models.Model):
	nome = models.CharField(max_length=100)
	codigo = models.BigIntegerField(verbose_name=u"Código", max_length=14, unique=True)

	class Meta:
		verbose_name = u'Produtos para Venda'
		verbose_name_plural = u'Produtos para Venda'
	
	def __unicode__(self):
		return "%s" % self.nome

	def retornar_produtos_vendiveis(self):
		return ProdutoVendaItem.objects.filter(produto_venda=self.id)


class ProdutoVendaItem(models.Model):
	produto_venda = models.ForeignKey(ProdutoVenda)
	produto = models.ForeignKey(Produto)
	quantidade_por_venda = models.PositiveIntegerField(default=1, blank=False)
	
	def __unicode__(self):
		return "%s" % self.produto.nome


class ProdutoPreco(models.Model):
	produto_venda = models.ForeignKey(ProdutoVenda)
	loja = models.ForeignKey(Loja)
	preco_loja = models.DecimalField(max_digits=5, decimal_places=2)
	
	def consumir(self, quantidade):
		produtos_vendiveis = self.produto_venda.retornar_produtos_vendiveis()
		for produto_venda_item in produtos_vendiveis:
			produto_loja_estoque, novo = ProdutoLojaEstoque.objects.get_or_create(loja=self.loja, produto=produto_venda_item.produto)
			produto_loja_estoque.remover(produto_venda_item.quantidade_por_venda * quantidade, self)
		
	def devolver(self, quantidade):
		produtos_vendiveis = self.produto_venda.retornar_produtos_vendiveis()
		for produto_venda_item in produtos_vendiveis:
			produto_loja_estoque, novo = ProdutoLojaEstoque.objects.get_or_create(loja=self.loja, produto=produto_venda_item.produto)
			produto_loja_estoque.adicionar(produto_venda_item.quantidade_por_venda * quantidade, self)
		
	def __unicode__(self):
		return self.loja.nome

class ProdutoCustoLoja(models.Model):
	loja = models.ForeignKey(Loja)
	produto = models.ForeignKey(Produto)
	ultimo_custo_por_unidade = models.DecimalField(max_digits=5, decimal_places=2, null=True)
	data_atualizacao =  models.DateTimeField(auto_now=True)

class ProdutoLojaEstoque(models.Model):
	produto = models.ForeignKey(Produto)
	loja = models.ForeignKey(Loja)
	quantidade_estoque = models.PositiveIntegerField(default=0, null=True)

	def adicionar(self, quantidade, objeto = None):
		self.quantidade_estoque += quantidade
		self.save()
		HistoricoEstoque.objects.create(
			loja=self.loja,
			produto=self.produto, 
			quantidade=quantidade,
			estoque_atual=self.quantidade_estoque,
			origem_app=objeto._meta.app_label if objeto else None,
			origem_model=objeto._meta.object_name if objeto else None,
			origem_id=objeto.id if objeto else None
		)

	def remover(self, quantidade, objeto = None):
		self.quantidade_estoque -= quantidade
		self.save()
		HistoricoEstoque.objects.create(
			loja=self.loja,
			produto=self.produto, 
			quantidade=quantidade*-1,
			estoque_atual=self.quantidade_estoque,
			origem_app=objeto._meta.app_label if objeto else None,
			origem_model=objeto._meta.object_name if objeto else None,
			origem_id=objeto.id if objeto else None
		)

	def __unicode__(self):
		return self.produto.nome

	class Meta:
		verbose_name = 'Estoque Atual'
		verbose_name_plural = 'Estoque Atual'


class HistoricoEstoque(models.Model):
	produto = models.ForeignKey(Produto)
	loja = models.ForeignKey(Loja)
	quantidade = models.IntegerField()
	estoque_atual = models.PositiveIntegerField(default=0)
	# usuario = models.ForeignKey(User)
	origem_app = models.CharField(max_length=100, null=True)
	origem_model = models.CharField(max_length=100, null=True)
	origem_id = models.BigIntegerField(max_length=10, null=True)
	data_cadastro = models.DateField(auto_now_add=True)
	hora_cadastro = models.TimeField(auto_now_add=True)

	def source(self):
		return get_model(self.origem_app, self.origem_model).objects.get(pk=self.origem_id)

	def __unicode__(self):
		return str(self.id)


class Entrada(models.Model):
	fornecedor = models.ForeignKey(Fornecedor)
	loja = models.ForeignKey(Loja)
	nota_fiscal = models.BigIntegerField(max_length=10, blank=False)
	data_nota_fiscal = models.DateField()
	valor_nota_fiscal = models.DecimalField(max_digits=6, decimal_places=2)
	data_cadastro = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return "%s %s" % (self.nota_fiscal, self.fornecedor.nome)

	def clean(self):
		if (self.pk > 0):
			raise ValidationError(u"Não é permitido atualizar") 

	def nota_fiscal_formatada(self):
		return "%0*d" % (6, self.nota_fiscal)

class EntradaProdutos(models.Model):
	entrada = models.ForeignKey(Entrada)
	produto = models.ForeignKey(Produto)
	quantidade_caixa = models.PositiveIntegerField()
	quantidade_unidade = models.PositiveIntegerField(blank=True, default=0)
	preco_custo_unidade = models.DecimalField(max_digits=5, decimal_places=2)
	preco_custo_caixa = models.DecimalField(max_digits=5, decimal_places=2)
	preco_custo_total = models.DecimalField(max_digits=6, decimal_places=2)
	margem_custo_unidade = models.DecimalField(verbose_name=u"Margem com última compra", max_digits=4, decimal_places=2, null=True)

	def clean(self):
		if (self.pk > 0):
			raise ValidationError(u"Não é permitido atualizar")

class TipoSaida(models.Model):
	nome = models.CharField(max_length=100)

	class Meta:
		verbose_name = u'Tipo de Saída'
		verbose_name_plural = u'Tipos de Saída'

	def __unicode__(self):
		return self.nome

class Saida(models.Model):
	usuario = models.ForeignKey(User)
	loja = models.ForeignKey(Loja);
	data_cadastro = models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = u'Saída'
		verbose_name_plural = u'Saídas'

	def __unicode__(self):
		return "%0*d" % (6, self.id)

	def clean(self):
		if (self.pk > 0):
			raise ValidationError(u"Não é permitido atualizar") 

class SaidaProdutos(models.Model):
	saida = models.ForeignKey(Saida)
	produto = models.ForeignKey(Produto)
	quantidade_unidade = models.PositiveIntegerField()
	motivo = models.ForeignKey(TipoSaida)

	def clean(self):
		if (self.pk > 0):
			raise ValidationError(u"Não é permitido atualizar")

@receiver(post_save, sender=SaidaProdutos)
def registar_saida_produtos(sender, **kwargs):
	obj = kwargs['instance']
	if kwargs['created']:
		estoque_produto_loja, novo = ProdutoLojaEstoque.objects.get_or_create(produto=obj.produto, loja=obj.saida.loja)
		estoque_produto_loja.remover(obj.quantidade_unidade, obj)


@receiver(post_save, sender=EntradaProdutos)
def adicionar_entrada_produtos(sender, **kwargs):
	obj = kwargs['instance']
	if kwargs['created']:
		obj.quantidade_unidade = obj.quantidade_caixa * obj.produto.quantidade_por_caixa
		obj.save()
		estoque_produto_loja, novo = ProdutoLojaEstoque.objects.get_or_create(produto=obj.produto, loja=obj.entrada.loja)
		estoque_produto_loja.adicionar(obj.quantidade_unidade, obj)
		
		produto_custo, novo = ProdutoCustoLoja.objects.get_or_create(produto=obj.produto, loja=obj.entrada.loja)
		produto_custo.ultimo_custo_por_unidade = obj.preco_custo_unidade
		produto_custo.save()
