# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Produto.quantidade_estoque'
        db.delete_column(u'estoque_produto', 'quantidade_estoque')

        # Adding field 'Produto.quantidade_por_venda'
        db.add_column(u'estoque_produto', 'quantidade_por_venda',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=1),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Produto.quantidade_estoque'
        db.add_column(u'estoque_produto', 'quantidade_estoque',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0),
                      keep_default=False)

        # Deleting field 'Produto.quantidade_por_venda'
        db.delete_column(u'estoque_produto', 'quantidade_por_venda')


    models = {
        u'estoque.entrada': {
            'Meta': {'object_name': 'Entrada'},
            'data_entrada': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'fornecedor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Fornecedor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loja': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Loja']"}),
            'nota_fiscal': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10', 'blank': 'True'})
        },
        u'estoque.entradaprodutos': {
            'Meta': {'object_name': 'EntradaProdutos'},
            'entrada': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Entrada']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'preco_custo_caixa': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'preco_custo_total': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'preco_custo_unidade': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'produto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Produto']"}),
            'quantidade_caixa': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'quantidade_unidade': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'blank': 'True'})
        },
        u'estoque.fornecedor': {
            'Meta': {'object_name': 'Fornecedor'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'estoque.loja': {
            'Meta': {'object_name': 'Loja'},
            'bairro': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'estoque.preco': {
            'Meta': {'object_name': 'Preco'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loja': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Loja']"}),
            'preco_loja': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'produto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Produto']"})
        },
        u'estoque.produto': {
            'Meta': {'object_name': 'Produto'},
            'codigo': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True', 'max_length': '14'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'quantidade_caixa_minima': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'quantidade_por_caixa': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'quantidade_por_venda': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.TipoProduto']"})
        },
        u'estoque.produtolojaestoque': {
            'Meta': {'object_name': 'ProdutoLojaEstoque'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loja': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Loja']"}),
            'produto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Produto']"}),
            'quantidade_estoque': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'null': 'True'})
        },
        u'estoque.tipoproduto': {
            'Meta': {'object_name': 'TipoProduto'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['estoque']