# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'FornecedorContato'
        db.create_table(u'estoque_fornecedorcontato', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fornecedor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['estoque.Fornecedor'])),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('contato', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('observacao', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'estoque', ['FornecedorContato'])

        # Adding field 'Fornecedor.tipo'
        db.add_column(u'estoque_fornecedor', 'tipo',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['estoque.TipoFornecedor']),
                      keep_default=False)

        # Adding field 'Fornecedor.cnpj'
        db.add_column(u'estoque_fornecedor', 'cnpj',
                      self.gf('django.db.models.fields.BigIntegerField')(max_length=14, null=True),
                      keep_default=False)

        # Adding field 'Fornecedor.responsavel'
        db.add_column(u'estoque_fornecedor', 'responsavel',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True),
                      keep_default=False)

        # Adding field 'Fornecedor.endereco'
        db.add_column(u'estoque_fornecedor', 'endereco',
                      self.gf('django.db.models.fields.CharField')(max_length=150, null=True),
                      keep_default=False)

        # Adding field 'Fornecedor.bairro'
        db.add_column(u'estoque_fornecedor', 'bairro',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True),
                      keep_default=False)

        # Adding field 'Fornecedor.cep'
        db.add_column(u'estoque_fornecedor', 'cep',
                      self.gf('django.db.models.fields.BigIntegerField')(max_length=10, null=True),
                      keep_default=False)

        # Adding field 'Fornecedor.data_cadastro'
        db.add_column(u'estoque_fornecedor', 'data_cadastro',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.datetime(2014, 7, 6, 0, 0), blank=True),
                      keep_default=False)

        # Adding field 'Loja.cnpj'
        db.add_column(u'estoque_loja', 'cnpj',
                      self.gf('django.db.models.fields.BigIntegerField')(max_length=14, null=True),
                      keep_default=False)

        # Adding field 'Loja.endereco'
        db.add_column(u'estoque_loja', 'endereco',
                      self.gf('django.db.models.fields.CharField')(max_length=150, null=True),
                      keep_default=False)

        # Adding field 'Loja.cep'
        db.add_column(u'estoque_loja', 'cep',
                      self.gf('django.db.models.fields.BigIntegerField')(max_length=10, null=True),
                      keep_default=False)

        # Adding field 'Loja.data_cadastro'
        db.add_column(u'estoque_loja', 'data_cadastro',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.datetime(2014, 7, 6, 0, 0), blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'FornecedorContato'
        db.delete_table(u'estoque_fornecedorcontato')

        # Deleting field 'Fornecedor.tipo'
        db.delete_column(u'estoque_fornecedor', 'tipo_id')

        # Deleting field 'Fornecedor.cnpj'
        db.delete_column(u'estoque_fornecedor', 'cnpj')

        # Deleting field 'Fornecedor.responsavel'
        db.delete_column(u'estoque_fornecedor', 'responsavel')

        # Deleting field 'Fornecedor.endereco'
        db.delete_column(u'estoque_fornecedor', 'endereco')

        # Deleting field 'Fornecedor.bairro'
        db.delete_column(u'estoque_fornecedor', 'bairro')

        # Deleting field 'Fornecedor.cep'
        db.delete_column(u'estoque_fornecedor', 'cep')

        # Deleting field 'Fornecedor.data_cadastro'
        db.delete_column(u'estoque_fornecedor', 'data_cadastro')

        # Deleting field 'Loja.cnpj'
        db.delete_column(u'estoque_loja', 'cnpj')

        # Deleting field 'Loja.endereco'
        db.delete_column(u'estoque_loja', 'endereco')

        # Deleting field 'Loja.cep'
        db.delete_column(u'estoque_loja', 'cep')

        # Deleting field 'Loja.data_cadastro'
        db.delete_column(u'estoque_loja', 'data_cadastro')


    models = {
        u'estoque.entrada': {
            'Meta': {'object_name': 'Entrada'},
            'data_cadastro': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data_nota_fiscal': ('django.db.models.fields.DateField', [], {}),
            'fornecedor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Fornecedor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loja': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Loja']"}),
            'nota_fiscal': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10', 'blank': 'True'}),
            'valor_nota_fiscal': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'})
        },
        u'estoque.entradaprodutos': {
            'Meta': {'object_name': 'EntradaProdutos'},
            'entrada': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Entrada']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'margem_custo_unidade': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '4', 'decimal_places': '2'}),
            'preco_custo_caixa': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'preco_custo_total': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'preco_custo_unidade': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'produto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Produto']"}),
            'quantidade_caixa': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'quantidade_unidade': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'blank': 'True'})
        },
        u'estoque.fornecedor': {
            'Meta': {'object_name': 'Fornecedor'},
            'bairro': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'cep': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10', 'null': 'True'}),
            'cnpj': ('django.db.models.fields.BigIntegerField', [], {'max_length': '14', 'null': 'True'}),
            'data_cadastro': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'endereco': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'responsavel': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.TipoFornecedor']"})
        },
        u'estoque.fornecedorcontato': {
            'Meta': {'object_name': 'FornecedorContato'},
            'contato': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'fornecedor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Fornecedor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'observacao': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'estoque.loja': {
            'Meta': {'object_name': 'Loja'},
            'bairro': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'cep': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10', 'null': 'True'}),
            'cnpj': ('django.db.models.fields.BigIntegerField', [], {'max_length': '14', 'null': 'True'}),
            'data_cadastro': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'endereco': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'estoque.preco': {
            'Meta': {'object_name': 'Preco'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loja': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Loja']"}),
            'preco_loja': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'produto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Produto']"}),
            'ultimo_preco_custo_unidade': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '5', 'decimal_places': '2'})
        },
        u'estoque.produto': {
            'Meta': {'object_name': 'Produto'},
            'codigo': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True', 'max_length': '14'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'quantidade_caixa_minima': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'quantidade_por_caixa': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'quantidade_por_venda': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.TipoProduto']"})
        },
        u'estoque.produtolojaestoque': {
            'Meta': {'object_name': 'ProdutoLojaEstoque'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loja': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Loja']"}),
            'produto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Produto']"}),
            'quantidade_estoque': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'null': 'True'})
        },
        u'estoque.tipofornecedor': {
            'Meta': {'object_name': 'TipoFornecedor'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'estoque.tipoproduto': {
            'Meta': {'object_name': 'TipoProduto'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['estoque']