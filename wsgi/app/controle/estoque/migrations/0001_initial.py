# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Fornecedor'
        db.create_table(u'estoque_fornecedor', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'estoque', ['Fornecedor'])

        # Adding model 'Loja'
        db.create_table(u'estoque_loja', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('bairro', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
        ))
        db.send_create_signal(u'estoque', ['Loja'])

        # Adding model 'TipoProduto'
        db.create_table(u'estoque_tipoproduto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'estoque', ['TipoProduto'])

        # Adding model 'Produto'
        db.create_table(u'estoque_produto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('codigo', self.gf('django.db.models.fields.BigIntegerField')(unique=True, max_length=14)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('tipo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['estoque.TipoProduto'])),
            ('quantidade_por_caixa', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('quantidade_caixa_minima', self.gf('django.db.models.fields.PositiveIntegerField')(default=1)),
            ('quantidade_estoque', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal(u'estoque', ['Produto'])

        # Adding model 'Preco'
        db.create_table(u'estoque_preco', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('produto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['estoque.Produto'])),
            ('loja', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['estoque.Loja'])),
            ('preco_loja', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2)),
        ))
        db.send_create_signal(u'estoque', ['Preco'])

        # Adding model 'Entrada'
        db.create_table(u'estoque_entrada', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fornecedor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['estoque.Fornecedor'])),
            ('loja', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['estoque.Loja'])),
            ('nota_fiscal', self.gf('django.db.models.fields.BigIntegerField')(max_length=10, blank=True)),
            ('data_entrada', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'estoque', ['Entrada'])

        # Adding model 'EntradaProdutos'
        db.create_table(u'estoque_entradaprodutos', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('entrada', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['estoque.Entrada'])),
            ('produto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['estoque.Produto'])),
            ('quantidade_caixa', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('quantidade_unidade', self.gf('django.db.models.fields.PositiveIntegerField')(default=0, blank=True)),
            ('preco_custo_unidade', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2)),
            ('preco_custo_caixa', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2)),
            ('preco_custo_total', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
        ))
        db.send_create_signal(u'estoque', ['EntradaProdutos'])


    def backwards(self, orm):
        # Deleting model 'Fornecedor'
        db.delete_table(u'estoque_fornecedor')

        # Deleting model 'Loja'
        db.delete_table(u'estoque_loja')

        # Deleting model 'TipoProduto'
        db.delete_table(u'estoque_tipoproduto')

        # Deleting model 'Produto'
        db.delete_table(u'estoque_produto')

        # Deleting model 'Preco'
        db.delete_table(u'estoque_preco')

        # Deleting model 'Entrada'
        db.delete_table(u'estoque_entrada')

        # Deleting model 'EntradaProdutos'
        db.delete_table(u'estoque_entradaprodutos')


    models = {
        u'estoque.entrada': {
            'Meta': {'object_name': 'Entrada'},
            'data_entrada': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'fornecedor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Fornecedor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loja': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Loja']"}),
            'nota_fiscal': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10', 'blank': 'True'})
        },
        u'estoque.entradaprodutos': {
            'Meta': {'object_name': 'EntradaProdutos'},
            'entrada': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Entrada']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'preco_custo_caixa': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'preco_custo_total': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'preco_custo_unidade': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'produto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Produto']"}),
            'quantidade_caixa': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'quantidade_unidade': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'blank': 'True'})
        },
        u'estoque.fornecedor': {
            'Meta': {'object_name': 'Fornecedor'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'estoque.loja': {
            'Meta': {'object_name': 'Loja'},
            'bairro': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'estoque.preco': {
            'Meta': {'object_name': 'Preco'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loja': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Loja']"}),
            'preco_loja': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'produto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Produto']"})
        },
        u'estoque.produto': {
            'Meta': {'object_name': 'Produto'},
            'codigo': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True', 'max_length': '14'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'quantidade_caixa_minima': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'quantidade_estoque': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'quantidade_por_caixa': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.TipoProduto']"})
        },
        u'estoque.tipoproduto': {
            'Meta': {'object_name': 'TipoProduto'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['estoque']