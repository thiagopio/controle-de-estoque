# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Entrada.nota_fiscal'
        db.alter_column(u'estoque_entrada', 'nota_fiscal', self.gf('django.db.models.fields.BigIntegerField')(max_length=10, null=True))

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Entrada.nota_fiscal'
        raise RuntimeError("Cannot reverse this migration. 'Entrada.nota_fiscal' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Entrada.nota_fiscal'
        db.alter_column(u'estoque_entrada', 'nota_fiscal', self.gf('django.db.models.fields.BigIntegerField')(max_length=10))

    models = {
        u'estoque.entrada': {
            'Meta': {'object_name': 'Entrada'},
            'data_cadastro': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data_nota_fiscal': ('django.db.models.fields.DateField', [], {}),
            'fornecedor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Fornecedor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loja': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Loja']"}),
            'nota_fiscal': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'valor_nota_fiscal': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'})
        },
        u'estoque.entradaprodutos': {
            'Meta': {'object_name': 'EntradaProdutos'},
            'entrada': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Entrada']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'margem_custo_unidade': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '4', 'decimal_places': '2'}),
            'preco_custo_caixa': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'preco_custo_total': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'preco_custo_unidade': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'produto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Produto']"}),
            'quantidade_caixa': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'quantidade_unidade': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'blank': 'True'})
        },
        u'estoque.fornecedor': {
            'Meta': {'object_name': 'Fornecedor'},
            'bairro': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'cep': ('django.db.models.fields.CharField', [], {'max_length': '8', 'null': 'True'}),
            'cnpj': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True'}),
            'data_cadastro': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'endereco': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'responsavel': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'sigla': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.TipoFornecedor']"})
        },
        u'estoque.fornecedorcontato': {
            'Meta': {'object_name': 'FornecedorContato'},
            'contato': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'fornecedor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Fornecedor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'observacao': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'estoque.loja': {
            'Meta': {'object_name': 'Loja'},
            'bairro': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'cep': ('django.db.models.fields.CharField', [], {'max_length': '8', 'null': 'True', 'blank': 'True'}),
            'cnpj': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'data_cadastro': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'endereco': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'estoque.preco': {
            'Meta': {'object_name': 'Preco'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loja': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Loja']"}),
            'preco_loja': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'produto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Produto']"}),
            'ultimo_preco_custo_unidade': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '5', 'decimal_places': '2'})
        },
        u'estoque.produto': {
            'Meta': {'object_name': 'Produto'},
            'codigo': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True', 'max_length': '14'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'quantidade_caixa_minima': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'quantidade_por_caixa': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'quantidade_por_venda': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.TipoProduto']"})
        },
        u'estoque.produtolojaestoque': {
            'Meta': {'object_name': 'ProdutoLojaEstoque'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loja': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Loja']"}),
            'produto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['estoque.Produto']"}),
            'quantidade_estoque': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'null': 'True'})
        },
        u'estoque.tipofornecedor': {
            'Meta': {'object_name': 'TipoFornecedor'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'estoque.tipoproduto': {
            'Meta': {'object_name': 'TipoProduto'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['estoque']