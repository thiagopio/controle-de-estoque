from django.test import TestCase
from controle.estoque.models import TipoProduto, TipoFornecedor, TipoSaida, Loja
from controle.estoque.models import Produto, Preco


class LojaTestCase(TestCase):
	fixtures = ['loja','user']

	def test_total_cadastrados(self):
		total_cadastrado = len(Loja.objects.all())
		self.assertEquals(total_cadastrado, 3)


class TipoFornecedorTestCase(TestCase):
	fixtures = ['tipofornecedor',]

	def test_total_cadastrados(self):
		total_cadastrado = len(TipoFornecedor.objects.all())
		self.assertEquals(total_cadastrado, 2)


class TipoProdutoTestCase(TestCase):
	fixtures = ['tipoproduto',]

	def test_total_cadastrados(self):
		total_cadastrado = len(TipoProduto.objects.all())
		self.assertEquals(total_cadastrado, 2)



class TipoSaidaTestCase(TestCase):
	fixtures = ['tiposaida',]

	def test_total_cadastrados(self):
		total_cadastrado = len(TipoSaida.objects.all())
		self.assertEquals(total_cadastrado, 3)


class ProdutoTestCase(TestCase):
	fixtures = ['loja','produto_com_preco']

	def test_total_produto_cadastrados(self):
		total_cadastrado = len(Produto.objects.all())
		self.assertEquals(total_cadastrado, 3)

	def test_total_preco_cadastrados(self):
		total_cadastrado = len(Preco.objects.all())
		self.assertEquals(total_cadastrado, 9)

	def test_preco_cadastrado_por_loja(self):
		produto = Produto.objects.get(pk=1)
		loja = Loja.objects.get(pk=1)
		produto_loja = Preco.objects.get(produto=produto, loja=loja)
		self.assertEquals(produto_loja.preco_loja, 2)