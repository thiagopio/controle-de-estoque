 # -*- coding: utf-8 -*-
from django.contrib import admin
from controle.estoque.models import ProdutoPreco, ProdutoVendaItem, ProdutoVenda
from controle.estoque.models import EntradaProdutos, Entrada, Loja, Produto, TipoProduto, TipoSaida
from controle.estoque.models import TipoFornecedor, Fornecedor, FornecedorContato, Saida, SaidaProdutos


class ProdutoVendaItemInline(admin.TabularInline):
	model = ProdutoVendaItem
	extra = 1

class ProdutoPrecoInline(admin.TabularInline):
	model = ProdutoPreco
	# extra = 1

class ProdutoVendaAdmin(admin.ModelAdmin):
	list_display = ('nome','codigo')
	inlines = [ProdutoVendaItemInline, ProdutoPrecoInline]

class EntradaProdutoInline(admin.TabularInline):
	model = EntradaProdutos
	fields = ['produto', 'quantidade_caixa', 'preco_custo_total', 'preco_custo_caixa', 'preco_custo_unidade', 'margem_custo_unidade']

class EntradaAdmin(admin.ModelAdmin):
	list_display = ('nota_fiscal_formatada', 'fornecedor', 'data_cadastro')
	inlines = [EntradaProdutoInline]
	actions = None

	def has_delete_permission(self, request, id = None):
		return False

	class Media:
	    js = ("/static/entrada_produtos.js","/static/formata-dinheiro.js")

class ProdutoAdmin(admin.ModelAdmin):
	list_display = ('nome', 'tipo', 'quantidade_por_caixa')
	ordering = ['nome',]

class FornecedorContatoInline(admin.TabularInline):
	model = FornecedorContato
	extra = 1

class FornecedorAdmin(admin.ModelAdmin):
	list_display = ('nome', 'sigla', 'tipo')
	inlines = [FornecedorContatoInline]
	list_filter = ('tipo',)
	ordering = ['sigla','nome']

class LojaAdmin(admin.ModelAdmin):
	readonly_fields = ('fundo_de_caixa',)
	list_display = ('nome', 'bairro', 'fundo_de_caixa')

class SaidaProdutosInline(admin.TabularInline):
	model = SaidaProdutos

class SaidaAdmin(admin.ModelAdmin):
	inlines = [SaidaProdutosInline]

	def has_delete_permission(self, request, id = None):
		return False


admin.site.register(TipoFornecedor)
admin.site.register(Fornecedor, FornecedorAdmin)
admin.site.register(Loja, LojaAdmin)
admin.site.register(TipoProduto)
admin.site.register(Produto, ProdutoAdmin)
admin.site.register(ProdutoVenda, ProdutoVendaAdmin)
admin.site.register(Entrada, EntradaAdmin)
admin.site.register(Saida, SaidaAdmin)
admin.site.register(TipoSaida)
