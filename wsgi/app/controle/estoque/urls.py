from django.conf.urls import patterns, url
from controle.estoque import views

urlpatterns = patterns('',
    # url(r'^cupom$', views.gerar_cupom),

    url(r'^servico/(?P<loja_id>\d+)/(?P<produto_id>\d+)/verificar_quantidade_e_custo', views.verificar_quantidade_e_custo),
)


